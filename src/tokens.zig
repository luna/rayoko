pub const TokenType = enum {
    // basic tokens
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    LeftSquare,
    RightSquare,
    Dot,
    Equal,
    Semicolon,
    Comma,
    Colon,
    Address,
    Pipe,
    QuestionMark,
    DollarSign,

    // math operators
    Plus,
    Minus,
    Star,
    Slash,
    Modulo,

    // one-two char tokens
    DotEqual,
    PlusPlus,
    PlusEqual,
    MinusEqual,
    ColonEqual,
    StarEqual,
    SlashEqual,

    // comparison ones
    EqualEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
    Bang,
    BangEqual,

    // complex types
    Integer,
    Float,
    String,
    Identifier,

    // keywords
    Break,
    Const,
    Continue,
    Defer,
    Else,
    Enum,
    Fn,
    For,
    Loop,
    Go,
    Goto,
    If,
    Import,
    In,
    Interface,
    Match,
    Module,
    Mut,
    Or,
    And,
    Return,
    Struct,
    Type,
    True,
    False,
    None,

    Println,
    Pub,
    Var,

    EOF,
};

pub const Token = struct {
    typ: TokenType,
    lexeme: []const u8,
    line: usize,
};
