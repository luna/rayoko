const std = @import("std");

pub fn report(line: usize, where: []const u8, ctx_opt: ?[]const u8, message: []const u8) void {
    if (ctx_opt) |ctx| {
        std.debug.warn("[line {}] Error{} on {}: {}", .{ line, where, ctx, message });
    } else {
        std.debug.warn("[line {}] Error{}: {}", .{ line, where, message });
    }
}

pub fn reportN(line: usize, message: []const u8) void {
    report(line, "", message);
}

pub fn reportFmt(line: usize, ctx_opt: ?[]const u8, comptime fmt: []const u8, args: anytype) void {
    if (ctx_opt) |ctx| {
        std.debug.warn("[line {}] Error on {}", .{ line, ctx });
    } else {
        std.debug.warn("[line {}] Error", .{line});
    }

    std.debug.warn(fmt, args);
    std.debug.warn("\n", .{});
}
