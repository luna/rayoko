const std = @import("std");

pub const llvm = @cImport({
    @cInclude("llvm-c/Core.h");
    @cInclude("llvm-c/ExecutionEngine.h");
    @cInclude("llvm-c/Target.h");
    @cInclude("llvm-c/TargetMachine.h");
    @cInclude("llvm-c/Analysis.h");
    @cInclude("llvm-c/BitWriter.h");

    @cDefine("_GNU_SOURCE", {});
    @cDefine("__STDC_CONSTANT_MACROS", {});
    @cDefine("__STDC_FORMAT_MACROS", {});
    @cDefine("__STDC_LIMIT_MACROS", {});
});

usingnamespace llvm;

pub const LLVMTypeList = std.ArrayList(llvm.LLVMTypeRef);
