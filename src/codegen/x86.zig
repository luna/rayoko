const std = @import("std");
const ast = @import("ast.zig");
const comp = @import("comp_ctx.zig");

pub const CompileError = error{
    EmitError,
    TypeError,
    Invalid,
};

pub const Codegenx86 = struct {
    allocator: *std.mem.Allocator,
    ctx: *comp.CompilationContext,

    current_function_name: ?[]const u8 = null,

    pub fn init(allocator: *std.mem.Allocator, ctx: *comp.CompilationContext) Codegen {
        return Codegen{
            .allocator = allocator,
            .ctx = ctx,
        };
    }

    // TODO

    pub fn gen(self: *Codegen, root: *ast.Node) !void {}
};
