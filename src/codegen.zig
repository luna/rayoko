pub const llvm = @import("codegen/llvm.zig");
pub const x86 = @import("codegen/x86.zig");

pub const CompileError = error{
    BackendError,
    EmitError,
    TypeError,
    Invalid,
};
